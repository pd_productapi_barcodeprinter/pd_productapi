package com.example.pd.product.productapi.batch;

import com.example.pd.product.productapi.entity.Product;
import org.springframework.batch.core.SkipListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@Component
public class ProductSkipListener implements SkipListener<Product, Product> {
    private static final List<String> skippedItems = new ArrayList<>();

    @Override
    public void onSkipInRead(Throwable t) {
        String skippedItem = "Line "+t.getMessage().charAt(23)+" Product Code: "+extractInputValues(t.getMessage()).get(1).replace("input=[","");
        skippedItems.add(skippedItem);
        System.out.println(skippedItem);
    }

    @Override
    public void onSkipInWrite(Product item, Throwable t) {
        System.out.println("Error during write. Skipped record: " + item + " due to: " + t.getMessage());
    }

    @Override
    public void onSkipInProcess(Product item, Throwable t) {
        System.out.println("Error during process. Skipped record: " + item + " due to: " + t.getMessage());
    }

    private static List<String> extractInputValues(String input) {
        int startIndex = input.indexOf('[') + 1;
        int endIndex = input.lastIndexOf(']');
        String values = input.substring(startIndex, endIndex);
        return Arrays.asList(values.split(","));
    }

    public static List<String> getSkippedItems() {
        List<String> currentItems = new ArrayList<>(skippedItems);
        skippedItems.clear();
        return currentItems;
    }
}
