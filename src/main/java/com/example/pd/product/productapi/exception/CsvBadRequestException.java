package com.example.pd.product.productapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CsvBadRequestException extends RuntimeException {
    public CsvBadRequestException(String message){
        super(message);
    }
}
