package com.example.pd.product.productapi.batch;

import com.example.pd.product.productapi.entity.Product;
import com.example.pd.product.productapi.repository.ProductRepository;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;


public class ProductWriter implements ItemWriter<Product> {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public void write(Chunk<? extends Product> chunk) throws Exception {
        productRepository.saveAll(chunk.getItems());
    }
}
