package com.example.pd.product.productapi.batch;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductStepExecutionListener implements StepExecutionListener {

    @Override
    public void beforeStep(StepExecution stepExecution) {
        // No action needed before step
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        long successfulWrites = stepExecution.getWriteCount();
        long failedWrites = stepExecution.getSkipCount();

        List<String> skippedItems = ProductSkipListener.getSkippedItems();

        stepExecution.getJobExecution().getExecutionContext().put("successfulWrites", successfulWrites);
        stepExecution.getJobExecution().getExecutionContext().put("failedWrites", failedWrites);
        stepExecution.getJobExecution().getExecutionContext().put("skippedItems", skippedItems);

        return stepExecution.getExitStatus();
    }
}
