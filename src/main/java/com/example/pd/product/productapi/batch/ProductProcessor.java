package com.example.pd.product.productapi.batch;

import com.example.pd.product.productapi.entity.Product;
import com.example.pd.product.productapi.repository.ProductRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;


public class ProductProcessor implements ItemProcessor<Product, Product> {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product process(Product item) throws Exception {
        if(productRepository.existsByProductCode(item.getProductCode())){
            Product product = productRepository.findByProductCode(item.getProductCode());
            product.setDescription(item.getDescription());
            product.setSell(item.getSell());
            //product.setCost(item.getCost());
            return product;
        }
        return item;
    }
}
