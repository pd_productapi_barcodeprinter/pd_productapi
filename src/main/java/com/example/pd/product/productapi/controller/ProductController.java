package com.example.pd.product.productapi.controller;

import com.example.pd.product.productapi.dto.ProductRequest;
import com.example.pd.product.productapi.dto.ProductResponse;
import com.example.pd.product.productapi.service.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(path="/api/v1/products")

public class ProductController {
    private final ProductService productService;

    @GetMapping
    public ResponseEntity<List<ProductResponse>> getProducts(){
        return ResponseEntity.ok(productService.getProducts());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ProductResponse> getProduct(@PathVariable String id){
        return ResponseEntity.ok(productService.getProduct(id));
    }

    @PostMapping(path = "/import")
    public ResponseEntity<String> importProducts(@RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(productService.importProducts(file));
    }

    @PostMapping
    public ResponseEntity<ProductResponse> addProduct(@Valid @RequestBody ProductRequest request){
        return ResponseEntity.created(productService.addProduct(request)).build();
    }

    @PutMapping
    public ResponseEntity<String> updateProduct(@Valid @RequestBody ProductRequest request){
        return ResponseEntity.ok(productService.updateProduct(request));
    }
}
