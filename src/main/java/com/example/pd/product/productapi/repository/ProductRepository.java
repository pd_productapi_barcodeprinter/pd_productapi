package com.example.pd.product.productapi.repository;

import com.example.pd.product.productapi.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Product findByProductCode(String productCode);
    Boolean existsByProductCode(String productCode);
}
