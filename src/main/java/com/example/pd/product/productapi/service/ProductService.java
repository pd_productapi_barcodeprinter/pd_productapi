package com.example.pd.product.productapi.service;

import com.example.pd.product.productapi.dto.ProductRequest;
import com.example.pd.product.productapi.dto.ProductResponse;
import com.example.pd.product.productapi.entity.Product;
import com.example.pd.product.productapi.exception.CsvBadRequestException;
import com.example.pd.product.productapi.exception.ProductAlreadyExistsException;
import com.example.pd.product.productapi.exception.ProductNotFoundException;
import com.example.pd.product.productapi.repository.ProductRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    @Autowired
    private JobExplorer jobExplorer;
    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private Job productJob;

    @Value("${temp.storage}")
    private String tempStorage;

    private final ProductRepository productRepository;

    public List<ProductResponse> getProducts(){
        List<Product> products = productRepository.findAll().stream().toList();
        return products.stream().map(this::mapToProductResponse).toList();
    }
    public ProductResponse getProduct(String productCode){
        try {
            Product product = productRepository.findByProductCode(productCode);
            return mapToProductResponse(product);
        }catch (Exception e) {
            throw new ProductNotFoundException("Product Code: "+productCode);
        }
    }

    public String importProducts(MultipartFile file) {
        try{
            if(!file.getContentType().toLowerCase().equals("text/csv")){
                throw new CsvBadRequestException("Please use correct csv format...");
            }
            Path filePath = FileSystems.getDefault().getPath(tempStorage+generateNewName(file.getOriginalFilename()));
            File fileToImport = new File(filePath.toString());

            file.transferTo(fileToImport);
            int columnCount = getNumberOfColumns(filePath);
            try {
                if(columnCount == 3) {
                    JobParameters jobParameters = new JobParametersBuilder()
                            .addString("inputFilePath", filePath.toString())
                            .toJobParameters();
                    JobExecution jobExecution = jobLauncher.run(productJob, jobParameters);
                    long successfulWrites = (Long) jobExecution.getExecutionContext().get("successfulWrites");
                    long failedWrites = (Long) jobExecution.getExecutionContext().get("failedWrites");
                    List<String> skippedItems = (List<String>) jobExecution.getExecutionContext().get("skippedItems");

                    StringBuilder failureItemsMessage = new StringBuilder();
                    for (String skippedItem : skippedItems) {
                        failureItemsMessage.append(skippedItem).append("\n");
                    }

                    return "Successful Writes: " + successfulWrites +
                            ", Failed Writes: " + failedWrites + ",\nFailed Items: \n"+failureItemsMessage;
                }else{
                    throw new CsvBadRequestException("Incorrect number of columns found in record: expected 3 actual "+columnCount);
                }
            }finally {
                Files.deleteIfExists(filePath);
            }
        }catch (Exception e){
            throw new CsvBadRequestException("Error reading CSV file: "+e.getMessage());
        }
    }

    private int getNumberOfColumns(Path filePath) throws IOException, CsvValidationException {
        try (BufferedReader reader = Files.newBufferedReader(filePath, StandardCharsets.UTF_8);
             CSVReader csvReader = new CSVReaderBuilder(reader).build()) {
            String[] header = csvReader.readNext();
            if (header != null) {
                return header.length;
            } else {
                throw new IllegalStateException("CSV file is empty or does not contain a header.");
            }
        }
    }

    public URI addProduct(ProductRequest request){
        if(!productRepository.existsByProductCode(request.getProductCode())) {
            Product product = Product.builder()
                    .productCode(request.getProductCode())
                    .description(request.getDescription())
                    .sell(request.getSell())
                    .build();
            var savedProduct = productRepository.save(product);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(savedProduct.getProductCode())
                    .toUri();
            return location;
        }else{
            throw new ProductAlreadyExistsException(String.format("Product Code %s already exists.",request.getProductCode()));
        }
    }

    public String updateProduct(ProductRequest request){
        try{
            Product product = productRepository.findByProductCode(request.getProductCode());
            product.setProductCode(request.getProductCode());
            product.setDescription(request.getDescription());
            product.setSell(request.getSell());
            //product.setCost(request.getCost());
            productRepository.save(product);
            return "Success";
        }catch (Exception e){
            throw new ProductNotFoundException("Product Code: "+request.getProductCode());
        }
    }

    private ProductResponse mapToProductResponse(Product product){
        return ProductResponse.builder()
                .productId(product.getProductId())
                .productCode(product.getProductCode())
                .description(product.getDescription())
                .sell(product.getSell())
                //.cost(product.getCost())
                .build();
    }

    private String generateNewName(String filePath){
        String fileName = filePath;
        String baseName = fileName.substring(0, fileName.lastIndexOf('.'));
        String extension = fileName.substring(fileName.lastIndexOf('.'));

        String newName = baseName + "_" + System.currentTimeMillis()+extension;

        return newName;
    }
}
