package com.example.pd.product.productapi.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {
    @NotNull(message = "Product Code cannot be empty.") @NotEmpty(message = "Product Code cannot be empty.")
    private String productCode;
    private String description;
    @PositiveOrZero(message = "Sell cannot be less than Zero")
    private double sell;
//    @PositiveOrZero(message = "Cost cannot be less than Zero.")
//    private double cost;
}
