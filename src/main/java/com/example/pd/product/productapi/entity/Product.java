package com.example.pd.product.productapi.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="product_tbl")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name="productId",
            updatable = false
    )
    private Long productId;

    @Column(
            name = "productCode",
            nullable = false,
            columnDefinition = "VARCHAR(50)"
    )
    private String productCode;

    @Column(
            name = "description",
            columnDefinition = "TEXT"
    )
    private String description;

    @Column(
            name = "sell",
            columnDefinition = "DECIMAL(10,2)"
    )
    private double sell;

//    @Column(
//            name = "cost",
//            columnDefinition = "DECIMAL(10,2)"
//    )
//    private double cost;


}
