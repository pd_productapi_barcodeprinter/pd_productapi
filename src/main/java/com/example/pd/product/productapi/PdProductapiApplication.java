package com.example.pd.product.productapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdProductapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdProductapiApplication.class, args);
	}

}
