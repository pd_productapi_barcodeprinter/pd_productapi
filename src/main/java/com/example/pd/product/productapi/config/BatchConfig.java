package com.example.pd.product.productapi.config;

import com.example.pd.product.productapi.batch.ProductProcessor;
import com.example.pd.product.productapi.batch.ProductSkipListener;
import com.example.pd.product.productapi.batch.ProductStepExecutionListener;
import com.example.pd.product.productapi.batch.ProductWriter;
import com.example.pd.product.productapi.entity.Product;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class BatchConfig {

    @Autowired
    private ProductStepExecutionListener productStepExecutionListener;

    @Bean
    public Job productReaderJob(JobRepository jobRepository, PlatformTransactionManager platformTransactionManager,FlatFileItemReader<Product> reader){
        return new JobBuilder("productReadJob", jobRepository)
                .incrementer(new RunIdIncrementer())
                .start(chunkStep(jobRepository,platformTransactionManager,reader))
                .build();
    }

    @Bean
    public Step chunkStep(JobRepository jobRepository, PlatformTransactionManager transactionManager, FlatFileItemReader<Product> reader){
        return new StepBuilder("productReaderStep", jobRepository).<Product, Product>chunk(10, transactionManager)
                .reader(reader)
                .processor(processor())
                .writer(writer())
                .faultTolerant()
                .skip(FlatFileParseException.class)
                .skipLimit(10000)
                .listener(new ProductSkipListener())
                .listener(productStepExecutionListener)
                .build();
    }

    @Bean
    @StepScope
    public ItemWriter<Product> writer(){
        return new ProductWriter();
    }

    @Bean
    @StepScope
    public ItemProcessor<Product, Product> processor(){
        return new ProductProcessor();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Product> reader(@Value("#{jobParameters[inputFilePath]}") String inputFilePath){
        return new FlatFileItemReaderBuilder<Product>()
                .name("productReader")
                .resource(new FileSystemResource(inputFilePath))
                .delimited()
                .names(new String[]{"productCode", "description", "sell"})
                .fieldSetMapper(fieldSet -> {
                    Product product = Product.builder()
                            .productCode(fieldSet.readString("productCode"))
                            .description(fieldSet.readString("description"))
                            .sell(convertStringToDouble(fieldSet.readString("sell")))
                            .build();
                    return product;
                })
                .linesToSkip(1)
                .build();
    }

    private Double convertStringToDouble(String value){
        try{
            return Double.parseDouble(value.replaceAll(",",""));
        }catch (NumberFormatException e){
            return null;
        }
    }
}
